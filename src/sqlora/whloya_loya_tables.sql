CREATE TABLE TAGS (
    TAG_ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
    TAG VARCHAR2(100),
    CONSTRAINT TAG_ID_PK PRIMARY KEY(TAG_ID)
    );
/   
    
-- Add comments to the systables     
COMMENT ON TABLE TAGS
IS
'���� ������������';
/
    
-- Add comments to the columns 
COMMENT ON COLUMN TAGS.tag
  is 'any tag that might be connected to any SKU';
/

CREATE TABLE SKU_TAGGED (
    SKU_TAGGED_ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
    SKU VARCHAR2(254 CHAR),  
    TAG VARCHAR2(100)    
    );
    
-- Add comments to the systables 
COMMENT ON TABLE SKU_TAGGED
IS
'������������ � ����';
/
    
-- Add comments to the columns 
COMMENT ON COLUMN SKU_TAGGED.SKU 
  IS '��� ��� ��� SKU';
/
COMMENT ON COLUMN SKU_TAGGED.TAG 
  IS '��� ��� SKU';
/
  

