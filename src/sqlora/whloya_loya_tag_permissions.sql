
-- privileges - only authenticated users
declare
 l_priv_roles owa.vc_arr;
 l_priv_patterns owa.vc_arr;
begin
  l_priv_patterns(1) := '/data/*';

  ords.define_privilege(
      p_privilege_name     => 'protected.data',
      p_roles              => l_priv_roles,
      p_patterns           => l_priv_patterns
  );
  commit;
end;
/
-- delete privileges
BEGIN
  ORDS.delete_privilege_mapping('protected.data',
                                '/data/*'
  );
  
  COMMIT;
END;
/