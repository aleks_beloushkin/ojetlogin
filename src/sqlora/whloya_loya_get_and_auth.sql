select  sku.SKU SKU, sku.NAME NAME , sku.CATEGORY_ID CATEGORY_ID from coper.gd_item sku
where sku.partner_id = 1
union select CAST(skucat.category_id AS VARCHAR2(254 CHAR)) SKU, skucat.NAME NAME, NULL CATEGORY_ID from  coper.GD_CATEGORY skucat 
where skucat.partner_id = 1;
/
-- enable ords privileges - only authenticated users
declare
 l_priv_roles owa.vc_arr;
 l_priv_patterns owa.vc_arr;
begin
  l_priv_patterns(1) := '/data/*';

  ords.define_privilege(
      p_privilege_name     => 'protected.data',
      p_roles              => l_priv_roles,
      p_patterns           => l_priv_patterns
  );
  commit;
end;
/
-- disable ords privileges for test authentication
BEGIN
  ORDS.delete_privilege_mapping('protected.data',
                                '/data/*'
  );
  
  COMMIT;
END;
/