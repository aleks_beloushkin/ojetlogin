-- for sku_tagged table
create or replace PROCEDURE update_sku_tagged (
   p_id       IN  SKU_TAGGED.SKU_TAGGED_ID%TYPE
  ,p_sku      IN  SKU_TAGGED.SKU%TYPE
  ,p_tag      IN  SKU_TAGGED.TAG%TYPE
)
AS
BEGIN
  UPDATE SKU_TAGGED
  SET TAG   = p_tag, SKU = p_sku   
  WHERE SKU_TAGGED_ID  = p_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/
create or replace PROCEDURE delete_sku_tagged (
      fk_id IN  SKU_TAGGED.TAG%TYPE
     ,p_id  IN  SKU_TAGGED.SKU_TAGGED_ID%TYPE
)
AS
BEGIN
  DELETE FROM SKU_TAGGED WHERE sku_tagged_id = p_id and tag = fk_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/
create or replace PROCEDURE create_sku_tagged (  
  p_sku      IN  SKU_TAGGED.SKU%TYPE
  ,p_tag      IN  SKU_TAGGED.TAG%TYPE
  ,p_sku_tagged_id  OUT SKU_TAGGED.SKU_TAGGED_ID%TYPE
)
AS
BEGIN
  INSERT INTO SKU_TAGGED (SKU, TAG )
  VALUES( p_sku, p_tag)
  RETURNING SKU_TAGGED_ID into p_sku_tagged_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/
create or replace PROCEDURE delete_sku_tagged_all (
      sku_id IN  SKU_TAGGED.SKU%TYPE
)
AS
BEGIN
  DELETE FROM SKU_TAGGED WHERE sku = sku_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;