create or replace PROCEDURE update_tag (
  p_id       IN  TAGS.TAG_ID%TYPE,
  p_tag      IN  TAGS.TAG%TYPE
)
AS
BEGIN
  UPDATE TAGS
  SET TAG   = p_tag   
  WHERE TAG_ID  = p_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/
create or replace PROCEDURE delete_tag (
    p_id       IN  TAGS.TAG_ID%TYPE
)
AS
BEGIN
  DELETE FROM TAGS WHERE tag_id = p_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/
create or replace PROCEDURE create_tag (  
  p_tag     IN  TAGS.TAG%TYPE,
  p_tag_id  OUT TAGS.TAG_ID%TYPE
)
AS
BEGIN
  INSERT INTO TAGS (  TAG )
  VALUES( p_tag)
  RETURNING TAG_ID into p_tag_id;
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
