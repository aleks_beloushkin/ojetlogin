/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
  'ojs/ojoffcanvas', 'ojs/ojdialog', 'ojs/ojswitch', 'ojs/ojbutton'],
  function(oj, ko) {
      
    function ControllerViewModel() {
        var self = this;

        // Media queries for repsonsive layouts
        var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
        self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
        var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
        self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
        self.navDataSource = ko.observable(); 
        self.routerData = ko.observable();
        

        self.serviceURL = 'https://appserv1:9002/ords/loya/v1/login';
        self.username = ko.observable("");
        self.password = ko.observable("");


        // User Info used in Global Navigation area
        self.userLogin = ko.observable("");      
        self.isLoggedIn = ko.observable(false);
        self.restSessionId = ko.observable("");
        self.defaultRouterData = {
                'login': {label: 'Login', isDefault: true}
              };
        self.defaultNavigationData = [ {name: 'Login', id: 'login',
                              iconClass: 'oj-navigationlist-item-icon demo-icon-font-24'}
              ];  
        self.navigationData = [ {name: 'Login', id: 'login',
                              iconClass: 'oj-navigationlist-item-icon demo-icon-font-24'}
        ]; 
              
        


        self.userLoggedIn = function() {

            var result = $.ajax({
                url: self.serviceURL,
                data: JSON.stringify({"username": "login"}),
                cache: false,
                async: false,
                xhrFields: { withCredentials: true },             
                type: 'POST',
                contentType: 'application/json',					                             
                dataType: 'json',                
                success: function (data, textStatus, jqXHR) {                                       

                  self.userLogin(data.username);
                  self.isLoggedIn(true);
                  self.restSessionId("");
                  self.routerData = data.router;   
                  return true;

                },          

                error: function (jqXHR, textStatus, errorThrown) {              

                  self.userLogin("");
                  self.isLoggedIn(false);
                  self.restSessionId("");  
                  return false;
                }                     

            });     

            return result.responseJSON;
        };
     
       if (self.userLoggedIn()) {

             self.username(null);
             self.password(null);     
             self.router = oj.Router.rootInstance;
             oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();     

             self.router.configure(                                
                 self.routerData                                             
             );

             self.navigationData = [];
             for (var key in self.routerData) {
                 if (self.routerData.hasOwnProperty(key)) { 
                     self.navigationData.push({ name: self.routerData[key].label, id: key, 
                     iconClass: 'oj-navigationlist-item-icon demo-icon-font-24'});
                 }
             }       

             self.navDataSource = new oj.ArrayTableDataSource(self.navigationData, {idAttribute: 'id'});     
             self.navDataSource.reset(self.navigationData, {idAttribute: 'id'});  

             oj.Router.sync();                  

       } else {
            // Router setup
            self.router = oj.Router.rootInstance;
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

            self.router.configure(self.defaultRouterData);
            self.navDataSource = new oj.ArrayTableDataSource(self.defaultNavigationData, {idAttribute: 'id'});
            self.navDataSource.reset(self.defaultNavigationData,{idAttribute: 'id'});            
            oj.Router.sync();
       }


        self.deleteCookies = function () {
                 var allcookies = document.cookie.split(";");

                 for (var i = 0; i < allcookies.length; i++) {
                      var cookie = allcookies[i];
                      var eqPos = cookie.indexOf("=");
                      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
             }
        };


        // Header
        // Application Name used in Branding Area
        self.appName = ko.observable(oj.Translations.getTranslatedString('appName'));
        self.copyright = ko.observable(oj.Translations.getTranslatedString('copyright'));
        self.about = ko.observable(oj.Translations.getTranslatedString('about'));
        self.dueDate = ko.observable(oj.Translations.getTranslatedString('dueDate'));

        self.incidentNumber = ko.observable(oj.Translations.getTranslatedString('incidentNumber'));      
        self.languageChecked = ko.observable();     


        // Drawer
        // Called by nav drawer option change events so we can close drawer after selection
        self.navChangeHandler = function (event, data) {
         if (data.option === 'selection' && data.value !== self.router.stateId()) {
             
                //self.toggleDrawer();
            
            }
        };

        // Close offcanvas on medium and larger screens
        self.mdScreen.subscribe(function() {oj.OffcanvasUtils.close(self.drawerParams);});
        self.drawerParams = {
            displayMode: 'push',
            selector: '#navDrawer',
            content: '#pageContent'
        };
        // Called by navigation drawer toggle button and after selection of nav drawer item
        self.toggleDrawer = function() {
            return oj.OffcanvasUtils.toggle(self.drawerParams);
        };


        // Dropdown menu states
        self.menuItemSelect = function (event, ui) {
          switch (ui.item.attr("id")) {
            case "pref":

              break;
            case "about":

              break;
            case "out":
              self.userLogin("");
              self.isLoggedIn(false);
              self.restSessionId("");                            
              $.ajax({
                url: 'https://appserv1:9002/ords/loya/signed-out',
                data: JSON.stringify({"username": "login"}),            
                cache: false,        
                xhrFields: { withCredentials: true },     
		type: 'GET',		          
                success: function (data, textStatus, jqXHR) { 
                     
                   
                     self.router.configure(self.defaultRouterData);
                     self.navDataSource = new oj.ArrayTableDataSource(self.defaultNavigationData, {idAttribute: 'id'});
                     self.navDataSource.reset(self.defaultNavigationData,{idAttribute: 'id'});
                     self.router.go().then(
                         function(result) {
                             if (result.hasChanged) {
                             }
                             else {
                                 oj.Router.sync();
                             }
                         },
                         function(error) {
                             console.log('Transition to default state failed: ' + error.message);
                         }
                     );     
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error" + " " + textStatus );
                    self.authenticationMessages.push({ summary: "Authentication failed", detail: "Invalid user ID or password", severity: oj.Message.SEVERITY_TYPE.ERROR });
                }
              });  
                     
              break;
            default:
          }
        };

        self.closePreferences = function() {
          $("#md1").ojDialog("close");
        }
        ;

        self.selection = ko.computed( function () {
            console.log(self.router.stateId());

            if (self.router.stateId() === 'dashboard') {
                return null;
            }

            return self.router.stateId();
        })
        ;


        self.selectHandler = function (event, ui) {
            self.router.go(ui.key);
        }
        ;

        self.link1Name = "About Oracle";
        self.link1Id = "aboutOracle";
        self.link1Url = "http://www.oracle.com/us/corporate/index.html#menu-about";

        self.link2Name = ko.observable(oj.Translations.getTranslatedString('contacts'));
        self.link2Id = "contactUs";
        self.link2Url = "http://www.oracle.com/us/corporate/contact/index.html";
        }

        return new ControllerViewModel();
    }
);
