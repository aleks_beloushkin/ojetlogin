define({
  "root": {
    "incidentNumber": "Номер инцидента",
    "dasboardMenu": "Задачи",
    "appName": "Тестовое приложение Oracle JET",
    "copyright": "Copyright © 2014, 2016 Oracle and/or its affiliates All rights reserved.",
    "about": "О нас",
    "contacts": "Контакты",
    "dueDate": "Due date"
   },
   "lt": true
});