/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore'
        , 'knockout'        
        , 'viewModels/common/skuController'
        , 'viewModels/common/tagsListController'
        , 'viewModels/common/skuTagsController'
        , 'viewModels/common/tagSKUsController'
        , 'jquery'
        , 'ojs/ojknockout'
        , 'ojs/ojknockout-validation'
        , 'ojs/ojmodel'
        , 'promise'
        , 'ojs/ojtable'
        , 'ojs/ojpagingcontrol'
        , 'ojs/ojcollectiontabledatasource'
        , 'ojs/ojselectcombobox'
        , 'ojs/ojarraydatagriddatasource'
        , 'ojs/ojinputtext'
    ],
 function(oj, ko, skuController, tagsListController, skuTagsController, tagSKUsController, $) {
  
    function TagsViewModel() {
      var self = this;
        // Below are a subset of the ViewModel methods invoked by the ojModule binding
        // Please reference the ojModule jsDoc for additionaly available methods.
      
        // Validation tracker
        self.tracker = ko.observable();
        //to provide a message about wrong password
        self.validationMessages = ko.observableArray([]);    

        self.currentSKU = ko.observable();
        self.skuModel= ko.observable();
        self.skusList = ko.observable();
        self.skusdatasource = ko.observable();
       
        
        self.tagModel= ko.observable();
        self.tagsList = ko.observable();
        self.tagsdatasource = ko.observable();
           
        self.tagModel= tagsListController.createObjectModel();
        self.tagsList = tagsListController.createObjectsCollection();           
    
        
        self.tagSKUModel= ko.observable();
        self.tagSKUsList = ko.observable();
        self.tagSKUsdatasource = ko.observable();        
        self.nameSearch = ko.observable('');
              
       
        self.searchSKU = function () {
            
            var searchString = self.nameSearch();
           
            if (searchString.length > 2) {
               self.skusList(skuController.createObjectsCollection(searchString));                  
            }
            else {
               self.skusList(skuController.createObjectsCollection()); 
            }
            self.skusdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(self.skusList()))); 
        };
        
        self.search = ko.computed(self.searchSKU);
        
        // event handler for reset button (for search field)
        self.clearClick = function (data, event) {
            
            self.nameSearch('');
            self.searchSKU();
            return true;
        };

        self.skuModel= skuController.createObjectModel();
        self.skusList(skuController.createObjectsCollection());              
        self.skusdatasource(new oj.PagingTableDataSource(new oj.CollectionTableDataSource(self.skusList())));   
      
      
        self.fetch = function (successCallBack) {                                        
                   self.skusList().fetch({
                       success: successCallBack,
                       error: function (jqXHR, textStatus, errorThrown) {
                           console.log('Error in fetch: ' + textStatus);
                       }
                   });
               };
   
        self.getRowTemplateSKUs = function(data, context) {              
                return 'rowTemplateSKUs';          
        };
        
        self.refreshTagSKUsTableData = function (sku) {
            self.tagSKUModel= skuTagsController.createObjectModel(sku);
            self.tagSKUsList = skuTagsController.createObjectsCollection(sku);              
            self.tagSKUsdatasource(new oj.CollectionTableDataSource(self.tagSKUsList));  
        };
        
        self.refreshTagSKUs = function (sku) {          
            self.currentSKU(sku);
            
            //here fetch too
            //
            
            self.refreshTagSKUsTableData(sku);
            
            
            var col = skuTagsController.createObjectsCollection(sku);  
            col.fetch(
                                {
                                    success:function (collection, response, options) {
                                        skuTagsData = collection;
                                        self.renderSkuTagsView(skuTagsData);
                                    }
                                    ,
                                    error: function (jqXHR, textStatus, errorThrown) {                                       
                                        console.log(textStatus  + " " + errorThrown);
                                    }
                                }
                    );
                             
                             
            self.renderSkuTagsView = function(skuTagsData) {
 
                var sdata = skuTagsData.toJSON();
                console.log(sdata);
                
                var selectionArr = [];
                for (var key in sdata) {
                    if (sdata.hasOwnProperty(key)) {
                        selectionArr.push(sdata[key].tag_name);
                    }
                }        
                
                self.selectVal(selectionArr);
            };
        };
        
        self.selectionListener = function (event, data){
            var tableId = data.currentTarget.id;
            var key = currentSelection(tableId);
            event.refreshTagSKUs(key);
        };
        
        function currentSelection(tableId)
        {
            var selectionObj = $("#"+tableId).ojTable("option", "currentRow");
            return selectionObj ? selectionObj.rowKey : null;
        };
                
        self.selectVal = ko.observableArray();
        self.optionData = ko.observableArray();
      
        
        /*
         * 
         * Блядство - нужно фетчить
         * 
         */
        var col = tagsListController.createObjectsCollection();  
        col.fetch(
                                {
                                    success:function (collection, response, options) {
                                        tagsData = collection;
                                        self.renderTagsView(tagsData);
                                    }
                                    ,
                                    error: function (jqXHR, textStatus, errorThrown) {                                       
                                        console.log(textStatus  + " " + errorThrown);
                                    }
                                }
                    );
                             
                             
        self.renderTagsView = function(tagsData) {
            // self.tasks is the Knockout view model structure
            self.optionData(tagsData.toJSON());           
        };
        
        
        self.saveTags = function() {
            
            console.log(this.selectVal());
            //TO DO:
            //here 
            //1. get ids ??
            var selectedIdsArray = [];
            var tagLabels = this.selectVal();
            var col = tagsListController.createObjectsCollection();  
            col.fetch(
                                    {
                                        success:function (collection, response, options) {
                                            tagsData = collection;
                                            saveTagsArray(tagsData);
                                        }
                                        ,
                                        error: function (jqXHR, textStatus, errorThrown) {                                       
                                            console.log(textStatus  + " " + errorThrown);
                                        }
                                    }
                        );
                             
                             
            var saveTagsArray = function(tagsData) {
                 // self.tasks is the Knockout view model structure
                var data = tagsData.toJSON(); 
                for (var key in tagLabels) {
                    if (tagLabels.hasOwnProperty(key)) {
                        for (var inkey in data) {
                            if (data.hasOwnProperty(inkey)) {
                                if (data[inkey].value == tagLabels[key])
                                    selectedIdsArray.push(data[inkey].id);
                            }   
                        }
                    }        
                };
                // -- work with ids array
                console.log(selectedIdsArray);
               
                //3. save new tags if they are chosen
                var saveDataFun =  function() {
                    selectedIdsArray.forEach(function(value, index, arr) {
                        console.log(parseInt(value));
                        // here - save the model
                        var model = tagSKUsController.createNew();
                        model.save({
                                    'tag': value,
                                    'sku': self.currentSKU()
                                    },                                    
                                    {
                                    success: function(myModel, response, options) {
                                       console.log(response);
                                       self.refreshTagSKUsTableData(self.currentSKU());
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus  + " " + errorThrown);
                                    }
                        });
                 
                   });
                   
                };
                
                //2. delete all tags for sku     
                skuTagsController.changeAllData(self.currentSKU(), saveDataFun);
                
                
            };  
            
        };
        
                
      
      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
      
      
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constrcuted
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    
    //var vm = new TagsViewModel();
    
    /*
     * Some additional code on create if we need it
     * 
     */
    
    //return vm;
    
    

    // Create a view model and apply it to the document body. This causes any  
    // ojComponents specified in the HTML data-bind to be initialized and their  
    // attributes evaluated using the view model.  
    //ko.applyBindings(new TagsViewModel(), document.body);
        
    return new TagsViewModel();    
  }
      
          
);
