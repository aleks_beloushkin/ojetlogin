/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore'
        , 'knockout'
        , 'viewModels/common/tagsController'
        , 'viewModels/common/tagSKUsController'
        , 'jquery'
        , 'ojs/ojknockout'
        , 'ojs/ojknockout-validation'
        , 'ojs/ojmodel'
        , 'promise'
        , 'ojs/ojtable'
        , 'ojs/ojpagingcontrol'
        , 'ojs/ojcollectiontabledatasource'
        , 'ojs/ojinputtext'],
 function(oj, ko, tagsController, tagSKUsController , $) {
  
    function DashboardViewModel() {
        var self = this;
        // Below are a subset of the ViewModel methods invoked by the ojModule binding
        // Please reference the ojModule jsDoc for additionaly available methods.
        
        // Validation tracker
        self.tracker = ko.observable();
        //to provide a message about wrong password
        self.validationMessages = ko.observableArray([]);    

        self.tagModel= ko.observable();
        self.tagsList = ko.observable();
        self.tagsdatasource = ko.observable();
        
        
        self.fetch = function (successCallBack) {
                   self.tagsList().fetch({
                       success: successCallBack,
                       error: function (jqXHR, textStatus, errorThrown) {
                           console.log('Error in fetch: ' + textStatus);
                       }
                   });
               };
           
        self.tagModel= tagsController.createObjectModel();
        self.tagsList = tagsController.createObjectsCollection();    
        
        //self.tagsdatasource = new oj.CollectionTableDataSource(self.tagsList);  
        self.tagsdatasource = new oj.PagingTableDataSource(new oj.CollectionTableDataSource(self.tagsList));  
        
        
        self.tagSKUModel= ko.observable();
        self.tagSKUsList = ko.observable();
        self.tagSKUsdatasource = ko.observable();
  
        this.getTagsRowTemplate = function(data, context) {
              var mode = context.$rowContext['mode'];

              if (mode === 'edit') {
                  return 'editRowTemplateTags';
              }
              else if (mode === 'navigation') {
                  return 'rowTemplateTags';
              }
        };
        
        this.getTagSKUsRowTemplate = function(data, context) {              
            return 'rowTemplateTagSKUs';          
        };
        
        
        
        


        self.addTag = function () {         
            
            var trackerObj = ko.utils.unwrapObservable(self.tracker);
            if (!this._showComponentValidationErrors(trackerObj)) {
                return;
            }     
            
          
            var Model = self.tagModel.clone();            
            if (Model.attributes['tag'] == null) {
                self.validationMessages.push({ summary: "Adding failed", detail: "Empty name", severity: oj.Message.SEVERITY_TYPE.ERROR });
                return;
            }
            self.tagModel.attributes['tag'] = null;
            $('#newTag').ojInputText("option", "value", null);                    
            Model.save(Model.attributes, {
                          contentType: 'application/json',
                          success: function(tagModel, response, options) {
                              console.log('save succesful!');
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              console.log("save failed : " + textStatus + " " + errorThrown );
                          }
                      }).then( function() {
                          self.tagsList.add(Model);                           
                          console.log('created');                                                                         
                          Model = null;      
                          
                      });
             //*/
            
        };
        
        self.addTagSKU = function () {         
            
            var trackerObj = ko.utils.unwrapObservable(self.tracker);
            if (!this._showComponentValidationErrors(trackerObj)) {
                return;
            }     
            
          
            var Model = self.tagSKUModel.clone();            
            if (Model.attributes['sku'] == null) {
                self.validationMessages.push({ summary: "Adding failed", detail: "Empty sku", severity: oj.Message.SEVERITY_TYPE.ERROR });
                return;
            }
            self.tagModel.attributes['sku'] = null;
            $('#newTagSKU').ojInputText("option", "value", null);                    
            Model.save(Model.attributes, {
                          contentType: 'application/json',
                          success: function(tagSKUModel, response, options) {
                              console.log('save succesful!');
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              console.log("save failed : " + textStatus + " " + errorThrown );
                          }
                      }).then( function() {
                          self.tagsList.add(Model);                           
                          console.log('created');                                                                         
                          Model = null;      
                          
                      });
             //*/
            
        };

        this.deleteTag = function(tag_id) {

            var myModel = this.tagsList.get(tag_id);        
            if (myModel) {
                self.tagsList.remove(myModel);
                myModel.destroy();
            }
            $('#Tags').ojTable('refresh');

        };
        
        
        this.deleteTagSKU = function(sku_tagged_id) {

            var myModel = this.tagSKUsList.get(sku_tagged_id);        
            if (myModel) {
                self.tagSKUsList.remove(myModel);
                myModel.destroy();
            }
            $('#TagSKUs').ojTable('refresh');

        };
                
        self._showComponentValidationErrors = function (trackerObj) {
            trackerObj.showMessages();
            if (trackerObj.focusOnFirstInvalid())
                return false;

            return true;
        };
                
        self.refreshTagSKUs = function (tagId) {            
            self.tagSKUModel= tagSKUsController.createObjectModel(tagId);
            self.tagSKUsList = tagSKUsController.createObjectsCollection(tagId);              
            self.tagSKUsdatasource( new oj.CollectionTableDataSource(self.tagSKUsList));  
        };
        
        self.selectionListener = function (event, data){
            var tableId = data.currentTarget.id;
            var key = currentSelection(tableId);
            event.refreshTagSKUs(key);
        };
        
        function currentSelection(tableId)
        {
            var selectionObj = $("#"+tableId).ojTable("option", "currentRow");
            return selectionObj ? selectionObj.rowKey : null;
        };
        
        /**
         * Optional ViewModel method invoked when this ViewModel is about to be
         * used for the View transition.  The application can put data fetch logic
         * here that can return a Promise which will delay the handleAttached function
         * call below until the Promise is resolved.
         * @param {Object} info - An object with the following key-value pairs:
         * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
         * @param {Function} info.valueAccessor - The binding's value accessor.
         * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
         * the promise is resolved
         */
        self.handleActivated = function(info) {
          // Implement if needed
        };

        /**
         * Optional ViewModel method invoked after the View is inserted into the
         * document DOM.  The application can put logic that requires the DOM being
         * attached here.
         * @param {Object} info - An object with the following key-value pairs:
         * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
         * @param {Function} info.valueAccessor - The binding's value accessor.
         * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
         */
        self.handleAttached = function(info) {
          // Implement if needed
        };


        /**
         * Optional ViewModel method invoked after the bindings are applied on this View. 
         * If the current View is retrieved from cache, the bindings will not be re-applied
         * and this callback will not be invoked.
         * @param {Object} info - An object with the following key-value pairs:
         * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
         * @param {Function} info.valueAccessor - The binding's value accessor.
         */
        self.handleBindingsApplied = function(info) {
          // Implement if needed
        };

        /*
         * Optional ViewModel method invoked after the View is removed from the
         * document DOM.
         * @param {Object} info - An object with the following key-value pairs:
         * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
         * @param {Function} info.valueAccessor - The binding's value accessor.
         * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
         */
        self.handleDetached = function(info) {
          // Implement if needed
        };
        }

        /*
        * Returns a constructor for the ViewModel so that the ViewModel is constrcuted
        * each time the view is displayed.  Return an instance of the ViewModel if
        * only one instance of the ViewModel is needed.
        */

        var vm = new DashboardViewModel();

        $(document).ready (function() {
          $('#Tags').on('ojbeforeroweditend', function (event, data) {
              var rowIdx = data.rowContext.status.rowIndex;
              vm.tagsdatasource.at(rowIdx).then(function(rowObj) {
                  var rowData = rowObj['data'];  
                  var myModel = vm.tagsList.get(rowData.tag_id);
                      myModel.save({ 'tag' : rowData.tag }, {
                          contentType: 'application/json',
                          success: function(myModel, response, options) {
                              console.log('update succesful!');
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              console.log("Update failed with: " + textStatus + " " + errorThrown );
                          }
                      }).then( function() {
                          console.log('changed: ' + rowData.tag_id + ' ' + rowData.tag);
                });

              });

            });            
          
        });



        return vm;
    }
);
