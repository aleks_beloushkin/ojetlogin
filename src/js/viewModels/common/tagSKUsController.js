define(['ojs/ojcore', 'knockout'], function (oj, ko) {

    function TagSKUsController() {
        var self = this;   
        var urlRoot = "https://appserv1:9002/ords/loya/data/skutagged";

        var parseFun = function (response) {
            return {             
                sku_tagged_id: response['sku_tagged_id'],
                tag: response['tag'],
                sku: response['sku']
            };        
        };
        
        function parseSaveFun(response){
            return {sku_tagged_id: response['sku_tagged_id'],
                    tag: response['tag'],
                    sku: response['sku']
            };
        };
        
        
        
        self.createObjectModel = function (tag_id) {
            var ObjectModel = oj.Model.extend({
                urlRoot: urlRoot +  "/" + tag_id
                , idAttribute: "sku_tagged_id"
                , parse: parseFun
                , parseSave: parseSaveFun
            });

            return new ObjectModel();
        };
        
        
        self.createNew = function () {
            var ObjectModel = oj.Model.extend({
                urlRoot: urlRoot
                , idAttribute: "sku_tagged_id"
                , parse: parseFun
                , parseSave: parseSaveFun
            });

            return new ObjectModel();
        };
        
        
        self.createObjectsCollection = function (tag_id) {
            var ObjectsCollection = oj.Collection.extend({
                url: urlRoot +  "?tag=" + tag_id
                ,customUrl: urlRoot + "/" + tag_id
                ,fetchSize: -1
                ,model: this.createObjectModel(tag_id)
            });

            return new ObjectsCollection();
        };

        /*
         * 
         * 
         *
        self.createObjectModel = function () {
            var ObjectModel = oj.Model.extend({
                urlRoot: urlRoot
                ,idAttribute: "sku_tagged_id"
                , parse: parseFun
                , parseSave: parseSaveFun
            });

            return new ObjectModel();
        }; 
        self.createObjectsCollection = function () {
            var ObjectsCollection = oj.Collection.extend({
                url: urlRoot
                ,fetchSize: -1
                ,model: this.createObjectModel()
            });

            return new ObjectsCollection();
        };
        */
        
        
        
    }

    return new TagSKUsController();
});