define(['ojs/ojcore', 'knockout'], function (oj, ko) {

    function SKUsController() {
        var self = this;
        

        self.myBasicAuth = function() {}; 
              self.myBasicAuth.prototype.getHeader = function () 
              {
                  //var headers = { withCredentials: true };
                  //var headers = {};
                  //headers['Authorization'] = 'Basic ' + btoa("testuser2:testuser2");
                  //return headers;
              };
              
        var mainURL = "https://appserv1:9002/ords/loya/data/sku";

        var parseFun = function (response) {
            return {             
                sku: response['sku'],
                name: response['name'],
                сategory_id: response["category_id"]
            };        
        };
        
        function parseSaveFun(response){
            return {  
                sku: response['sku'],
                name: response['name'],
                сategory_id: response["category_id"]
            };
        };
        
        self.createObjectModel = function (searchString) {
            var ObjectModel = oj.Model.extend({
                urlRoot: mainURL
                , idAttribute: "sku"
                , parse: parseFun  
                , parseSave: parseSaveFun
            });

            return new ObjectModel();
        };

        self.createObjectsCollection = function (searchString) {
            var ObjectsCollection = oj.Collection.extend({
                url: mainURL  + (searchString ? "?q={\"name\":{\"$instr\": \"" + searchString + "\"}}" : "")          
                ,fetchSize: 10
                ,model: this.createObjectModel()
                //,oauth: self.myBasicAuth()
            });

            return new ObjectsCollection();
        };
        
        
        
    }

    return new SKUsController();
});