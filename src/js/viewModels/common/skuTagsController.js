define(['ojs/ojcore', 'knockout'], function (oj, ko) {

    function SkuTagsController() {
        var self = this;   
        var urlRoot = "https://appserv1:9002/ords/loya/data/skutags";

        var parseFun = function (response) {
            return {             
                sku_tagged_id: response['sku_tagged_id'],
                tag: response['tag'],
                tag_name: response['tag_name'],
                sku: response['sku']
            };        
        };
         
        function parseSaveFun(response){
            return {sku_tagged_id: response['sku_tagged_id'],
                    tag: response['tag'],
                    tag_name: response['tag_name'],
                    sku: response['sku']
            };
        };
        
        
        
        self.createObjectModel = function (sku) {
            var ObjectModel = oj.Model.extend({
                urlRoot: urlRoot +  "/" + sku
                , idAttribute: "sku_tagged_id"
                , parse: parseFun
                , parseSave: parseSaveFun
            });

            return new ObjectModel();
        };
        
        
        self.createObjectsCollection = function (sku) {
            var ObjectsCollection = oj.Collection.extend({
                url: urlRoot + "/" + sku              
                ,fetchSize: -1
                ,model: this.createObjectModel(sku)
            });

            return new ObjectsCollection();
        };
        
        self.changeAllData = function (sku, saveFun) {
          return $.ajax({
                    type: "DELETE",
                    headers:{ withCredentials: true },
                    url: urlRoot + "/" + sku
                })
            .done(saveFun)
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Delete failed with: " + textStatus + " " + errorThrown );
            });
        };
        
        
       

            
        
    }

    return new SkuTagsController();
});