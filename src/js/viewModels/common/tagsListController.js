define(['ojs/ojcore', 'knockout'], function (oj, ko) {

    function TagsListController() {
        var self = this;
        var urlTags = "https://appserv1:9002/ords/loya/data/tags";



        var parseFun = function (response) {
            return {          
                id: response['tag_id'],
                value: response['tag'],
                label: response['tag']
            };        
        };
        
       
        
        self.createObjectModel = function () {
            var ObjectModel = oj.Model.extend({
                urlRoot: urlTags
                ,idAttribute: "tag_id"
                , parse: parseFun                
            });

            return new ObjectModel();
        };

        self.createObjectsCollection = function () {
            var ObjectsCollection = oj.Collection.extend({
                url: urlTags
                ,fetchSize: -1
                ,model: this.createObjectModel()
            });

            return new ObjectsCollection();
        };
    }

    return new TagsListController();
});