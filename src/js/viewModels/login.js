/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Main content module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojmodel', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojknockout-validation'],
  function(oj, ko) {
   /**
    * The view model for the main content view template.  Please note that since
    * this example uses ojModule binding, you do not need to call ko.applyBindings
    * like the JET Cookbook examples.  ojModule handles applying bindings for its
    * associated view.
    */
 
    function LoginContentViewModel() {
        var self = this;
        
        //self.serviceURL = 'https://appserv1:9002/ords/testuser1/greetings/example';
        self.serviceURL = 'https://appserv1:9002/ords/loya/v1/login';
        self.username = ko.observable("");
        self.password = ko.observable("");
        self.tracker = ko.observable();
        //to provide a message about wrong password
        self.authenticationMessages = ko.observableArray([]);        
         
        self.login = function () {
            var trackerObj = ko.utils.unwrapObservable(self.tracker);
            if (!this._showComponentValidationErrors(trackerObj)) {
                return;
            }     
            
            var rootViewModel = ko.dataFor(document.getElementById('mainContent'));
            
            $.ajax({
                url: rootViewModel.serviceURL,
                data: JSON.stringify({"username": "login"}),            
                cache: false,        
                beforeSend: function (xhr) {                        
                        xhr.setRequestHeader ("Authorization",'Basic ' + btoa(self.username() + ":" + self.password()));                          
                },
		type: 'POST',
		contentType: 'application/json',					                             
                dataType: 'json',                
                success: function (data, textStatus, jqXHR) {     
                    
                    var header = jqXHR.getResponseHeader('Set-Cookie');                  
                    if (data.router !== null) {
                     
                        self.username(null);
                        self.password(null); 
                        rootViewModel.isLoggedIn('true');       
                        rootViewModel.routerData = data.router;
                       
                        rootViewModel.router.configure(                                
                            rootViewModel.routerData                                             
                        );
                
                        //Navigation setup - must be from router data 
                        rootViewModel.navigationData = [];
                        for (var key in rootViewModel.routerData) {
                            if (rootViewModel.routerData.hasOwnProperty(key)) {
                                rootViewModel.navigationData.push({ name:  rootViewModel.routerData[key].label , id: key });
                            }
                        }                             
                           
                        
                        rootViewModel.navDataSource.reset(rootViewModel.navigationData, {idAttribute: 'id'});
                        rootViewModel.userLogin(data.username);
                        rootViewModel.isLoggedIn('true');                        
                        oj.Router.sync();                        
                        
                    }
                   
                    else {
                        console.log("can't find any user rights");
                        self.authenticationMessages.push({ summary: "Authentication failed", detail: "User have right login but no rights to access", severity: oj.Message.SEVERITY_TYPE.ERROR });                        
                    }        
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //this._showComponentValidationErrors(trackerObj);
                    console.log("401 Unauthorized" + " " + textStatus );
                    self.authenticationMessages.push({ summary: "Authentication failed", detail: "Invalid user ID or password", severity: oj.Message.SEVERITY_TYPE.ERROR });
                }
            });              
        };
                
        self._showComponentValidationErrors = function (trackerObj) {
            trackerObj.showMessages();
            if (trackerObj.focusOnFirstInvalid())
                return false;

            return true;
        };
            
    }

    /**
     * This example returns a view model instance, but can instead return a constructor function
     * which will be invoked to create a view model instance for each module reference.
     * This instance example will be used as a singleton whenever this module is referenced.
     * Please see the 'ViewModel's Lifecycle' section of the ojModule doc for more info.
     */
    return new LoginContentViewModel();
});